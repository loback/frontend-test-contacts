import React from 'react';
import {Route} from 'react-router-dom';
import Container from './components/contacts.container'
import ContactList from './components/contacts.list';
import ContactView from './components/contacts.view';
import ContactEdit from './components/contacts.edit';
import ContactAdd from './components/contacts.add';
import ContactDelete from './components/contacts.delete';




export const Routes = (
    <Container>
        <Route component={ContactList} path='/' exact/>
        <Route component={ContactAdd} path='/contact/add' />
        <Route component={ContactEdit} path='/contact/edit/:id'/>
        <Route component={ContactView} path='/contact/get/:id' />
        <Route component={ContactDelete} path='/contact/delete/:id' />
    </Container>
)