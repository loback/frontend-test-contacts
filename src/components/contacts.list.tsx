import React from 'react';
import {Paper, Typography, IconButton, Link, Grid } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons'
import * as q from '../query';
import { Query } from 'react-apollo';


const ContactList = (props:any) => (
    <Query query={q.getContacts} fetchPolicy='network-only'>
      {({ loading, error, data }) => {

        return (
          <React.Fragment>
            {data && data.contacts &&
              <React.Fragment>
                {data.contacts.map((contact: any) => (
                  <Paper key={contact.id} elevation={2} style={{padding: 10}}>
                    <Grid
                      container
                      direction="row"
                      justify="flex-start"
                      alignItems="flex-start"
                    >
                      <Grid item xs={9}>
                        <Typography variant="h5" component="h3">
                          <Link href="#" onClick={() => props.history.push(`/contact/get/${contact.id}`)}>
                              {contact.name}
                          </Link>
                        </Typography>
                        <Typography component="p">
                          {contact.email}
                        </Typography>
                      </Grid>
                      <Grid item xs>
                        <IconButton onClick={()=> props.history.push(`/contact/edit/${contact.id}`)}>
                            <Edit />
                        </IconButton>
                        <IconButton onClick={() => props.history.push(`/contact/delete/${contact.id}`)}>
                            <Delete />
                        </IconButton>
                      </Grid>
                    </Grid>
                  </Paper>  
                ))}
              </React.Fragment>
            }
          </React.Fragment>
        )
          
      }}
   </Query>
)	

export default ContactList
