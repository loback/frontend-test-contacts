import React from 'react';
import { 
	AppBar,
	Grid,
	SvgIcon,
	Toolbar } from '@material-ui/core';
import './contacts.css';
import { NavLink } from 'react-router-dom';

class Container extends React.Component {

  render(){
    return (
      <React.Fragment>
        <Grid container justify='flex-start' direction="column">
          <Grid container justify='center'>
            <AppBar position="static" className="AppBar">
              <Toolbar>
                <SvgIcon>
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 3h-1V1h-2v2H8V1H6v2H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm6 12H6v-1c0-2 4-3.1 6-3.1s6 1.1 6 3.1v1z"/></svg>                
                </SvgIcon>
                <div className="AppBarTitle">
                  Contacts
                </div>
                <Grid container justify='flex-end' className="AppBarLinks">
                  <NavLink to='/'>Home</NavLink>
                  <NavLink to='/contact/add'>Add</NavLink>
                </Grid>
              </Toolbar>
              </AppBar>
            </Grid>
            <Grid container justify='flex-start' spacing={8} >
          </Grid>
        </Grid>
        <main>
          <Grid container justify='center'>
            <Grid item xs={6} className='main-container'>
              {this.props.children} 
            </Grid>
          </Grid>
        </main>
      </React.Fragment>
    )  
  }
  
}

export default Container;