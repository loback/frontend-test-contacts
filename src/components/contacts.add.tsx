import React, { useState } from 'react';
import {Paper, TextField, Button, Grid } from '@material-ui/core';
import * as q from '../query';
import { Mutation } from '@apollo/react-components';


const ContactAdd = (props: any) => {

  const initialInput = {name: '', email: ''};
  const [input, setInput] = useState(initialInput);

  return (
    <Mutation mutation={q.addContact}>
      {(ContactAdd, { data }) => (
            <Paper style={{ padding: '20px'}}>
              <Grid container spacing={8}>
                <form
                  onSubmit={e => {
                    e.preventDefault();
                    // TODO - Email validation
                    // TODO - simple name validation
                    const contact = {id: '', name: input.name, email: input.email};
                    ContactAdd({ variables: { contact } });
                    props.history.push('/');
                  }}
                >
                  <TextField style={{marginRight: '10px'}}
                      id='name'
                      label='Name'
                      className='input'
                      value={input.name}
                      margin='normal'
                      variant='outlined'
                      onChange={(e) => setInput({ ...input, name: e.target.value })}
                  />
                  <TextField
                      id='email'
                      label='Email'
                      className='input'
                      value={input.email}
                      margin='normal'
                      variant='outlined'
                      onChange={(e) => setInput({ ...input, email: e.target.value })}
                  />
                  <Button type="submit">Add a new contact</Button>
                </form>
              </Grid>
            </Paper>  
      )}
    </Mutation>
  )
} 

export default ContactAdd