import React from 'react';
import { Typography, CardContent, Card, CardHeader, Avatar } from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import * as q from '../query';

const ContactView = (props: any) => {

  const { id } = props.match.params;
  const { data } = useQuery(q.getContact, { variables: { id: id } });

  const contact = data && data.contact ? { ...data.contact } : {};

  return (
    <Card elevation={2}>
      <CardHeader
        avatar={
          <Avatar aria-label="avatar" >
            {contact.name && contact.name.charAt(0).toUpperCase()}
          </Avatar>
        }
        title={contact.name}
      />
      <CardContent>
        <Typography gutterBottom>
          Name: {contact.name}
        </Typography>
        <Typography gutterBottom>
          Email: {contact.email}
        </Typography>
        <Typography gutterBottom>
          Id: {contact.id}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default ContactView;