import React from 'react';
import {Paper, Button, Grid } from '@material-ui/core';
import * as q from '../query';
import {useMutation} from '@apollo/react-hooks';

const ContactDelete = (props: any) => {

  const delContact = useMutation(q.deleteContact)[0];
  const id = props.match.params.id;

  const deleteContact = () => {
    delContact({ variables: { id } });
    props.history.push('/');
  }

  const deleteCancel = () => {
    props.history.push('/');
  }
    

  return (
    <Paper style={{ padding: '20px' }}>
      <Grid container spacing={8}>
        <Grid item xs={12}>
          DO you really want to delete contact #{id}?
        </Grid>
        <Grid item xs={12}>
          <Button onClick={deleteContact} variant='text' color='primary'>
              YES
          </Button>
          <Button onClick={deleteCancel} variant='text' color='secondary'>
              NO
          </Button>
        </Grid>
      </Grid>
    </Paper>
  )
} 

export default ContactDelete