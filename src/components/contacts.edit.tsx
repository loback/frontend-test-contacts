import React, { useState } from 'react';
import {Paper, TextField, Button, Grid } from '@material-ui/core';
import * as q from '../query';
import { Mutation } from '@apollo/react-components';
import { Query } from 'react-apollo';


const ContactEdit = (props: any) => (
  <Query query={q.getContact} variables={{id: props.match.params.id}}>
    {({ loading, error, data }) => {
      const id = props.match.params.id;
      const initialInput = data && data.contact ? data.contact : {email: '', name: ''};
      const [input, setInput] = useState(initialInput);

      if (loading) return <p>Loading...</p>;


      return (
        <Mutation mutation={q.updateContact}>
          {(ContactEdit, { data }) => (
            <Paper style={{ padding: '20px'}}>
              <Grid container spacing={8}>
                <form
                  onSubmit={e => {
                    e.preventDefault();
                    // TODO - VALIDATION email and name
                    const contact = {id: id, name: input.name, email: input.email};
                    ContactEdit({ variables: { contact } });
                    props.history.push('/');
                  }}
                >
                  <TextField style={{ marginRight: '10px'}}
                      id='name'
                      label='Name'
                      className='input'
                      value={input.name}
                      margin='normal'
                      variant='outlined'
                      onChange={(e) => setInput({ ...input, name: e.target.value })}
                  />
                  <TextField
                      id='email'
                      label='Email'
                      className='input'
                      value={input.email}
                      margin='normal'
                      variant='outlined'
                      onChange={(e) => setInput({ ...input, email: e.target.value })}
                  />
                  <Button type="submit">EDIT</Button>
                </form>
              </Grid>
            </Paper>  
          )}
        </Mutation>
      )
    }}
  </Query>
)

export default ContactEdit